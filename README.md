# Open Door

This chal is the intro chal to any web CTF. It's super easy.

* Desc: This challenge is an open door; show us you know how to find the key.
* Flag: `TUCTF{f1r5t_fl46_345135t_fl46}`
* Hint: How would you view the source code of a file?

## How To:
1. Look at the source code of `index.html`, the flag is there. This can be done by using the browser tools on whatever browser you have, right clicking and viewing source, or wget/curling the page.
